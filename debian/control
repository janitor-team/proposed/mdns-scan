Source: mdns-scan
Section: utils
Priority: optional
Maintainer: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper (>= 11)
Standards-Version: 4.2.1
Homepage: https://github.com/alteholz/mdns-scan
Vcs-Browser: https://salsa.debian.org/alteholz/mdns-scan
Vcs-Git: https://salsa.debian.org/alteholz/mdns-scan.git

Package: mdns-scan
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Scan for mDNS/DNS-SD services published on the local network
 mdns-scan is a tool for scanning for mDNS/DNS-SD services published
 on the local network.
 .
 It works by issuing a mDNS PTR query to the special RR
 _services._dns-sd._udp.local for retrieving a list of all currently
 registered services on the local link.
 .
 mDNS/DNS-SD is part of Apple's Zeroconf strategy (a.k.a. Bonjour).
 The avahi-daemon package is an implementation of mDNS/DNS-SD.
